import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { inject } from '@angular/core';
import Swal from 'sweetalert2';

export const roleGuard: CanActivateFn = (route, state) => {
  const authService: AuthService = inject(AuthService);
  const router: Router = inject(Router);
  if (!authService.isAuthenticated()) {
    router.navigate(['/login']);
    return false;
  }
  let role = route.data['role'] as string;
  console.log(role);
  if (authService.hasRole(role)) {
    return true;
  }
  Swal.fire('Acceso denegado', `Hola ${authService.usuario.userName} no tiene acceso a éste recurso!`, 'warning');
  router.navigate(['/dashboard']);
  return false;
};   
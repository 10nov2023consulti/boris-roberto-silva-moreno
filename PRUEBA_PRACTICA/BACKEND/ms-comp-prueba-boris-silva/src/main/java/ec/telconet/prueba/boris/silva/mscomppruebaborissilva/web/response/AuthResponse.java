package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.response;

import lombok.Data;

@Data
public class AuthResponse {
    private String token;
    private String userName;
    private Object principal;
}
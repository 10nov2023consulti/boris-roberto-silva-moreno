import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule,Routes } from '@angular/router';


import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './User/usuarios/usuarios.component';
import { LoginComponent } from './User/login/login.component';
import { PListComponent } from './Productos/components/p-list/p-list.component';
import { ListComponent } from './User/list/list.component';
import { FormComponent } from './User/form/form.component';
import { ArchivosComponent } from './archivos/archivos.component';
import { SearchBoxComponent } from './User/components/search-box/search-box.component';
import { UserPageComponent } from './User/pages/user-page/user-page.component';
import { TableComponent } from './User/pages/table/table.component';
import { PFormComponent } from './Productos/components/p-form/p-form.component';
import { HeaderComponent } from './header/header.component';
import { authGuard } from './User/guards/auth.guard';
import { roleGuard } from './User/guards/role.guard';


const routes : Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'user/form', component: FormComponent, 
  canActivate:[authGuard, roleGuard], data: {role: 'ADMIN'}},
  {path: 'user', component: UserPageComponent},
  {path: 'import', component: ArchivosComponent},
  {path: 'dashboard/user/form/:id', component: FormComponent, 
  canActivate:[authGuard, roleGuard], data: {role: 'ADMIN'}},
  {path: 'producto', component: PListComponent},
  {path: 'producto/form', component: PFormComponent, 
   canActivate:[authGuard, roleGuard], data: {role: 'ADMIN'}},

  {path: 'producto/form/:id', component: PFormComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UsuariosComponent,
    PListComponent,
    ListComponent,
    FormComponent,
    ArchivosComponent,
    SearchBoxComponent,
    UserPageComponent,
    TableComponent,
    PFormComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
   
    HttpClientModule,
    RouterModule.forRoot(routes)
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

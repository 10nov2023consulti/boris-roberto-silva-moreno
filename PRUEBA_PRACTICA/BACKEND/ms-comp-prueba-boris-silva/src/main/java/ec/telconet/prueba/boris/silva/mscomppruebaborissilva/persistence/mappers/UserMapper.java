package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.mappers;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.UserEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {RolUserMapper.class})
public interface UserMapper {
    @Mappings({
            @Mapping(source = "idUser", target = "idUser"),
            @Mapping(source = "userName", target = "userName"),
            @Mapping(source = "password", target = "password"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "estado", target = "status"),

            @Mapping(source = "rolUserEntities", target = "rolUsers"),
    })
    User toUser(UserEntity userEntity);
    List<User> toUsers(List<UserEntity> userEntities);

    @InheritInverseConfiguration
    @Mappings({



    })
    UserEntity toUserEntity(User user);
}
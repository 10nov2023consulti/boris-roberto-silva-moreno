package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

import java.io.Serializable;

@Embeddable
@Data
public class RolUserPK implements Serializable {
    @Column(name="id_user")
    private Integer idUser;
    @Column(name="id_rol")
    private String idRol;
}

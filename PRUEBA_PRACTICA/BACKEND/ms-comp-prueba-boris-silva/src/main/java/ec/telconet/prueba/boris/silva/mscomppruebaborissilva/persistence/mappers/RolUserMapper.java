package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.mappers;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.RolUser;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.RolUserEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {RolMapper.class})
public interface RolUserMapper {
    @Mappings({
            @Mapping(source = "id.idRol", target = "idRol"),
            @Mapping(source = "rolEntity", target = "rol")
    })
    RolUser toRolUser(RolUserEntity rolUserEntity);
    @InheritInverseConfiguration
    @Mappings({

            @Mapping(target = "id.idUser", ignore = true),
            @Mapping(target = "userEntity", ignore = true),

    })
    RolUserEntity toRolUserEntity(RolUser rolUser);
}
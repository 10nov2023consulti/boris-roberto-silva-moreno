package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud.ProductoEntityCrudRepository;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.ProductoEntity;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductResponseService {
    @Autowired
    private ProductoEntityCrudRepository productoEntityCrudRepository;

    @Autowired
    private ApiService apiService;

    @PostConstruct
    public void guardar() throws JsonProcessingException {
        List<ProductoEntity>   productoEntities = apiService.getAll();

        System.out.println(productoEntities);

        productoEntityCrudRepository.saveAll(productoEntities);
    }
}

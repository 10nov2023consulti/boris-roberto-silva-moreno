package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "roles")
@Data
public class RolEntity {

    @Id
    @Column(name = "id_rol", nullable = false)
    private String name;


    private boolean active;




}

import { Component, Input } from '@angular/core';
import { User } from '../class/user';
import { UserService } from '../services/user.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

@Input()
 public user: User = new User();

  public users!: User[];
  paginador: any;
 
   constructor(private userService: UserService,
    public auth: AuthService,
     private acticateRoute:ActivatedRoute){}
     
   ngOnInit(): void {
     
    this.getUsers();
   }
 
   getUsers():void{
 
     this.acticateRoute.paramMap.subscribe( params => {
      
       let page: number = +params.get('page')!;
       if(!page){
         page = 0;
       }
        
       this.userService.getUsers(page).subscribe(
         response => {
           this.users = response.content as User[];
           console.log(this.users)
           this.paginador = response ;
 
         }
       );
     });
     
   }

   delete(user: User): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Estas Seuro ? ',
      text: `¿Seguro que deseas eliminar el cliente ${user.userName}  ?` ,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Eliminar',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.delete(user.idUser).subscribe(
          response => {
            this.users = this.users.filter(cli => cli !== user)
            swalWithBootstrapButtons.fire(
              'Cliente Eliminado!',
              `Cliente ${user.userName} eliminado con éxito`,
              'success'
            )
          }
        );
       
      } 
    })
 
 }  

}



import { Component } from '@angular/core';
import { AuthService } from '../User/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  constructor(private auth: AuthService){


  }

  public nombre: string = this.auth.usuario.userName;


}

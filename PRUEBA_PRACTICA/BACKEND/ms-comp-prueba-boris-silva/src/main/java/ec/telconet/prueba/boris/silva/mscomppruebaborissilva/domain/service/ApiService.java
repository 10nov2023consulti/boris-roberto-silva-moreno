package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.api.Product;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.api.ProductResponseList;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.ProductoEntity;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiService {

    String apiUrl = "https://dummyjson.com/products";

    @Autowired
    private RestTemplate clienteRest;


    public List<ProductoEntity> getAll() throws JsonProcessingException {
        String jsonResponse = clienteRest.getForObject(apiUrl, String.class);
        ObjectMapper objectMapper = new ObjectMapper();

        ProductResponseList productResponseList = objectMapper.readValue(jsonResponse, ProductResponseList.class);

        List<Product> products = productResponseList.getProducts();

        List<ProductoEntity> productoEntities = products.stream().map(
                product -> {
                    ProductoEntity productoEntity = new ProductoEntity();

                    productoEntity.setNombre(product.getTitle());
                    productoEntity.setPrecio(product.getPrice());
                    productoEntity.setCantidad(product.getStock());
                    productoEntity.setImagen(product.getThumbnail());
                    productoEntity.setEstado(true);

                    return productoEntity;

                }
        ).collect(Collectors.toList());

        return productoEntities;
    }
}

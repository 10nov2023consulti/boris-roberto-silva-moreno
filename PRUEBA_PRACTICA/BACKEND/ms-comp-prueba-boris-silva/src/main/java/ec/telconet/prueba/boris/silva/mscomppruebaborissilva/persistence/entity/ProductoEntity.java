package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Entity
@Table(name = "productos")
@Data
public class ProductoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

   @NotEmpty
    private String nombre;
    private Integer cantidad;
    private double precio;
    private String imagen;
    private boolean estado;
}

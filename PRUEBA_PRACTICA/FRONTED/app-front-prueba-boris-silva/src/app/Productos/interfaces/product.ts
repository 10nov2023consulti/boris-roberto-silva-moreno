export class Product {
    id!:       number;
    nombre!:   string;
    cantidad!: number;
    precio!:   number;
    imagen!:   string;
    estado!:   boolean;
}
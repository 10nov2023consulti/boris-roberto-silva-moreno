package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.controller;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.dto.LoginDto;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service.UserService;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.config.JwtUtil;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.response.AuthResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;


    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto){

        UsernamePasswordAuthenticationToken login = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());

        Authentication authentication = authenticationManager.authenticate(login);



        System.out.println(authentication.isAuthenticated());
        System.out.println(authentication.getPrincipal());
        Object object =  authentication.getPrincipal();

        User user = userService.findByUserName(authentication.getName());

        Map<String, String> claims = new HashMap<>();



        List<String> roles = user.getRolUsers().stream().map(
                rolUser -> rolUser.getRol().getName()
        ).collect(Collectors.toList());



        Integer idUser = user.getIdUser();

        String jwt = jwtUtil.create(loginDto.getUsername(), claims, roles,  idUser);

        AuthResponse response = new AuthResponse();
        response.setToken(jwt);
        response.setUserName(authentication.getName());
        //response.setPrincipal(object);

        return ResponseEntity.ok().header(HttpHeaders.AUTHORIZATION, jwt).body(response);
    }
}
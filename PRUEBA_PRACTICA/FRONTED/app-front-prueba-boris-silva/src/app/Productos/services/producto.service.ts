import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { Content, Producto } from '../interfaces/producto.interface';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlEndpoint: string = environment.url+'api/product';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private http: HttpClient) { }

  getLast():Observable<any>{
    return this.http.get<Content[]>(`${this.urlEndpoint}/last/5`)
  }

  getProductoById(idProducto: number):Observable<Content>{
    return this.http.get<Content>(`${this.urlEndpoint}/id/${idProducto}`)
  }

  create(content: Content): Observable<Content>{
    return this.http.post<Content>(`${this.urlEndpoint}/save`, content, {headers: this.httpHeaders});

  }

  update(content: Content):Observable<Content>{
    return this.http.put<Content>(`${this.urlEndpoint}/update`, content, {headers: this.httpHeaders}).pipe(
      catchError(e => {

        if(e.status==400){
          
          return throwError( () => e);
        }
       
        console.log(e.error.mensaje);
        
        Swal.fire('Error al editar producto', `${e.error.mensaje}`, 'error');
        return throwError( () => e);
      })
    );

  }

  delete(id: number): Observable<Content>{
    return this.http.delete<Content>(`${this.urlEndpoint}/delete/${id}`, {headers: this.httpHeaders}).pipe(
      catchError(e => {
       
        console.log(e.error.mensaje);
        
        Swal.fire('Error al Eliminar producto', `${e.error.mensaje}`, 'error');
        return throwError( () => e);
      })
    )
  }


}

import { Component } from '@angular/core';
import { AuthService } from '../User/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {


  constructor(public authService: AuthService,
    private router: Router){

  }

  signOut():void{
    Swal.fire('Logout', `Hola ${this.authService.usuario.userName}, has cerrado sesión con éxito!`);
    this.authService.logout();
    console.log("cerrar session");

    this.router.navigate(['/login']);
  }
}

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../User/services/user.service';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.css']
})
export class ArchivosComponent {

  selectedFile!: File;

  constructor(private userService: UserService) {}

  onFileChange(event: any) {
    this.selectedFile = event.target.files[0];
  }

  uploadFile() {
    const formData: FormData = new FormData();
    formData.append('file', this.selectedFile, this.selectedFile.name);

    this.userService.importUsers(formData).subscribe(
      response => {
        console.log('Usuarios importados correctamente', response);
      },
      error => {
        console.error('Error al importar usuarios', error);
      }
    );
  }

}

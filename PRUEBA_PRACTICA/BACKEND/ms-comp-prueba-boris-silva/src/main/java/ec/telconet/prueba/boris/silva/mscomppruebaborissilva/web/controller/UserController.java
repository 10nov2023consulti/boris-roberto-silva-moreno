package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.controller;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @GetMapping("/all")
    public ResponseEntity<List<User>> findAll(){
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/all/page/{page}")
    public ResponseEntity<Page<User>> findAll(@PathVariable("page") int page){
        Pageable pageable = PageRequest.of(page, 5);

        return new ResponseEntity<>(userService.finAll(pageable), HttpStatus.OK);
    }
    @GetMapping("/id/{id}")
    public ResponseEntity<User> findById(@PathVariable("id") int idUser){
        return new ResponseEntity<>(userService.findById(idUser), HttpStatus.OK);
    }
    @GetMapping("name/{name}")
    public ResponseEntity<User> findByUserName(@PathVariable("name") String username){
        return new ResponseEntity<>(userService.findByUserName(username), HttpStatus.OK);
    }
    @PostMapping("/save")
    public ResponseEntity<?> save(@Valid @RequestBody User user, BindingResult result) {
        System.out.println("Hola");
        Map<String, Object> response = new HashMap<>();

        System.out.println(result.getAllErrors());
        if(result.hasFieldErrors()){
            System.out.println("Tiene errores");
        }
        if (result.hasErrors()){

            List<String> errors = result.getFieldErrors().stream()
                    .map(e -> "El campo " + e.getField()+" "+ e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
    }

    @PostMapping("/import")
    public ResponseEntity<String> importUsers(@RequestParam("file") MultipartFile file) {
        System.out.println("Controlador import");

        try {
            userService.importUsers(file);
            System.out.println("Controlador import");
            return ResponseEntity.ok("Usuarios importados correctamente.");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    @PutMapping("update")
    public ResponseEntity<User> update(@RequestBody User user){
        return new ResponseEntity<>(userService.update(user), HttpStatus.OK);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int idUser){
        userService.deleteById(idUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

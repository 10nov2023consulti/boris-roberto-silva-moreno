package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain;

import lombok.Data;

@Data
public class Rol {

    private String name;

    private boolean active;
}

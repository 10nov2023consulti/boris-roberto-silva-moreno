import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent {


  @Input()
  public placeHolder: string = '';

  @ViewChild('txtInput')
  txtInput! : ElementRef<HTMLInputElement>

  @Output()
  public onValue = new EventEmitter<string>();

  searchTerm():void{
    const term = this.txtInput.nativeElement.value ;
    this.onValue.emit(term);

  }
}

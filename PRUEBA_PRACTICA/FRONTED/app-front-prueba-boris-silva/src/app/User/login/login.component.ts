import { Component, OnInit } from '@angular/core';
import { User } from '../class/user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  titulo: string = 'Por favor iniciar session';
  usuario: User;

  constructor(private authService:AuthService,
    private router:Router){
    this.usuario = new User;
  }
  ngOnInit(): void {
    if(this.authService.isAuthenticated()){
      Swal.fire('Login', `Hola ${this.authService.usuario.userName} ya estas autenticado`, 'info')
      this.router.navigate(['/dashboard'])
    }
  }


  login(): void{
    console.log(this.usuario);
    if(this.usuario.userName == null || this.usuario.password == null ){
      Swal.fire('Error Login', 'Username o password vacio!', 'error')
      return;
    }


    this.authService.login(this.usuario).subscribe(response => {
      console.log(response);
  

   this.authService.guardarUsuario(response.token);
   // console.log('Guardar user')
   this.authService.guardarToken(response.token)
      
      let usaurio = this.authService.usuario ; 
      
      this.router.navigate(['/dashboard']);
      Swal.fire('Login', `Hola ${usaurio.userName}, has iniciado sessón con exito`, 'success');
    });
  }


}

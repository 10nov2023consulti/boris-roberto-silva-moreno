import { Component } from '@angular/core';
import { User } from '../../class/user';
import { UserService } from '../../services/user.service';
import jsPDF, { jsPDFAPI } from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent {

  public byUserName: string = 'Buscar por user name';

  public user!: User;

  constructor(private userService: UserService){}

  searcByUserName(term : string): void{
   
    this.userService.getUser(term)
      .subscribe( user => {
        this.user = user;
      } );
  }



}

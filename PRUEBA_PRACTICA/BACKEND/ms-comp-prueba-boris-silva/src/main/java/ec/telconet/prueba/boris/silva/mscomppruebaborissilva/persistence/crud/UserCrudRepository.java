package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserCrudRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByIdUser(Integer idUser);
    Optional<UserEntity> findByUserName(String username);
}

package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Entity
@Table(name= "users")
@Data
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Integer idUser;

    @NotEmpty
    @Size(min = 8)
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[@#$%^&+=]).*$")
    private String password;

    @Pattern(regexp = "^[A-Z]+$")
    @NotEmpty
    @Column(name = "user_name")
    private String userName;

    @Email()
    @Column(name = "email")
    private String email;
    @Column(name = "estado")
    private boolean estado;


    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<RolUserEntity> rolUserEntities;

}

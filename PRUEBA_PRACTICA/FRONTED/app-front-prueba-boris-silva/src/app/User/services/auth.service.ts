import { Injectable } from '@angular/core';
import { User } from '../class/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _usuario!: User | null;
  private _token!: string | null;

  constructor(private http:HttpClient, 
    private userService:UserService) { }
  

  public get usuario(): User{
      if(this._usuario != null){
        return this._usuario;
      } else if(this._usuario == null && sessionStorage.getItem('usuario') != null){
       this._usuario =  JSON.parse(sessionStorage.getItem('usuario')!) as User ;
       return this._usuario;
      }
      return new User();
  }  
  public get token(): string{
    if(this._token != null){
      return this._token;
    } else if(this._token == null && sessionStorage.getItem('usuario') != null){
     this._token =  sessionStorage.getItem('token')! ;
     return this._token;
    }
    return null!;
  }  

  login(usuario: User):Observable<any>{
    
    const urlEndpoint = 'http://localhost:8090/api/api/auth/login'

    const credentials = {
      username: usuario.userName,
      password: usuario.password
    }

    const httpHraders = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.post<any>(urlEndpoint, credentials, {headers: httpHraders})
  }

  guardarUsuario(token: string): void {
      let user = this.obtenerDatosUsuario(token);
      this._usuario = new User();
      this._usuario.userName = user.sub;
    
      this._usuario.id = user.idUser;
    
      
      
      this._usuario.rolUsers = user.roles;

      sessionStorage.setItem('usuario', JSON.stringify(this._usuario) )
      localStorage.setItem('usuario', JSON.stringify(this._usuario));

      console.log('Se guardo el user')

  }

  guardarToken(token: string):void{
    this._token = token;
    localStorage.setItem('token', token)
    sessionStorage.setItem('token', token);
  }

  obtenerDatosUsuario(token: string):any{

    if(token != null){
      return JSON.parse(atob(token.split(".")[1]));
    }
    return null ;
   
  
  }

  isAuthenticated(): boolean{
    let payload = this.obtenerDatosUsuario(this.token);
    console.log(payload);

    if(payload != null && payload.sub.length > 0){
      return true;
    }
    return false;
  }

  hasRole(role: string): boolean{
    if(this.usuario.rolUsers.includes(role)){
      return true;
    }
    return false;
  }

  logout():void{

    this._token = null;
    this._usuario = null;
    localStorage.clear();
    sessionStorage.clear();

  }

}

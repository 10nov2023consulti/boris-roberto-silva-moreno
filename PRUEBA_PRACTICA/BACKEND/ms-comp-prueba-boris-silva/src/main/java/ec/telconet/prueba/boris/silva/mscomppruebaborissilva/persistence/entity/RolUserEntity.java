package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "rol_user")
@Data
public class RolUserEntity {

    @EmbeddedId
    private RolUserPK id;



    @MapsId("idUser")
    @ManyToOne
    @JoinColumn(name = "id_user", insertable = false, updatable = false)
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "id_rol", insertable = false, updatable = false)
    private RolEntity rolEntity;

}
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../class/user';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlEndpoint: string = environment.url+'api/user';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});




  constructor(private http:HttpClient,
    ) { }




  getUsers(page: number):Observable<any>{
    return this.http.get<User[]>(`${this.urlEndpoint}/all/page/${page}`)
  }

  getUserById(idUser: number):Observable<User>{
    return this.http.get<User>(`${this.urlEndpoint}/id/${idUser}`)
  }
  create(user: User): Observable<User> {
    return this.http.post<User>(this.urlEndpoint+'/save', user, {headers: this.httpHeaders});
  }

  getUser(username: string):Observable<User>{
    return this.http.get<User>(`${this.urlEndpoint}/name/${username}`)
  }

  update(user: User, ): Observable<User>{
    return this.http.put<User>(`${this.urlEndpoint}/update`, user, {headers: this.httpHeaders }).pipe(
      catchError(e => {

        if(e.status==400){
          
          return throwError( () => e);
        }
       
        console.log(e.error.mensaje);
        
        Swal.fire('Error al editar cliente', `${e.error.mensaje}`, 'error');
        return throwError( () => e);
      })
    );


}

delete(id: number): Observable<User>{
  return this.http.delete<User>(`${this.urlEndpoint}/delete/${id}`, {headers: this.httpHeaders}).pipe(
    catchError(e => {
     
      console.log(e.error.mensaje);
      
      Swal.fire('Error al Eliminar cliente', `${e.error.mensaje}`, 'error');
      return throwError( () => e);
    })
  )
}


importUsers(file: FormData): Observable<any> {
  return this.http.post(`${this.urlEndpoint}/import`, file);
}
}

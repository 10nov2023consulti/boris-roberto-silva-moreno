insert into public.roles (active, id_rol)
values (true, 'ADMIN');

insert into public.roles (active, id_rol)
values (true, 'USER');



insert into users (estado, id_user, email, password, user_name)
values (true, 10, 'email@email.com',
        '$2a$12$K/VSUqK0MC2xDtmeeyCyL.bBGdAAFABwd7k51NuaeNd2oITgPJpNO', 'BORIS');

insert into users (estado, id_user, email, password, user_name)
values (true, 11, 'email@email.com',
        '$2a$12$K/VSUqK0MC2xDtmeeyCyL.bBGdAAFABwd7k51NuaeNd2oITgPJpNO', 'KEVIN');



insert into rol_user (id_user, id_rol)
values (10, 'ADMIN');

insert into rol_user (id_user, id_rol)
values (11, 'USER');
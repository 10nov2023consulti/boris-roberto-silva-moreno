import { Component, OnInit } from '@angular/core';
import { Content, Producto } from '../../interfaces/producto.interface';
import { ProductoService } from '../../services/producto.service';
import { Product } from '../../interfaces/product';
import Swal from 'sweetalert2';
import { AuthService } from 'src/app/User/services/auth.service';

@Component({
  selector: 'app-p-list',
  templateUrl: './p-list.component.html',
  styleUrls: ['./p-list.component.css']
})
export class PListComponent implements OnInit{

  public productos!: Content[];
  constructor(private productoService: ProductoService,
    public auth: AuthService){}
  ngOnInit(): void {
    this.getProducts();
  }

getProducts():void{
  this.productoService.getLast().subscribe(
    response => {
      this.productos = response.content as Content[]; 
    }
  )
}

delete(product: Product): void {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Estas Seuro ? ',
    text: `¿Seguro que deseas eliminar el producto ${product.nombre}  ?` ,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, Eliminar',
    cancelButtonText: 'No, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      this.productoService.delete(product.id).subscribe(
        response => {
          this.productos = this.productos.filter(cli => cli !== product)
          swalWithBootstrapButtons.fire(
            'Producto ELiminado!',
            `Cliente ${product.nombre} eliminado con éxito`,
            'success'
          )
        }
      );
     
    } 
  })

}  


}

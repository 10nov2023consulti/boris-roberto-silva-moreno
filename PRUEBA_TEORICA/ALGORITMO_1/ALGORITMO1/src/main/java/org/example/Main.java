package org.example;

import java.util.HashMap;
/**
 * Este es el metodo principal del algoritmo .
 * Manda a llamar al metodo que verifica los numeros que cumplen con el target
 * Se realizan las pruebas del algoritmo

 *
 */
public class Main {
    public static void main(String[] args) {

        int[] nums1 = {2, 7, 11, 15};
        int target1 = 9;
        /** Llamamo el metodo y el resultado lo almacenamos el la varialbre result1*/
        int[] result1 = twoSum(nums1, target1);
        System.out.println("Ejemplo #1: [" + result1[0] + ", " + result1[1] + "]");

        // Ejemplo #2
        int[] nums2 = {3, 2, 4};
        int target2 = 6;
        /** Llamamo el metodo y el resultado lo almacenamos el la varialbre result2*/
        int[] result2 = twoSum(nums2, target2);
        System.out.println("Ejemplo #2: [" + result2[0] + ", " + result2[1] + "]");

       // Ejemplo #3
        int[] nums3 = {3, 3, -3, -3, 10};
        int target3 = -6;

        /** Llamamo el metodo y el resultado lo almacenamos el la varialbre result3*/
        int[] result3 = twoSum(nums3, target3);
        System.out.println("Ejemplo #3: [" + result3[0] + ", " + result3[1] + "]");
    }



    /**
     * Este metodo encuentra los numeros que sumados son iguales al target , y devuele un arrgelo con esos numeros.
     *
     * @param nums int[] , arreglo de enteros.
     * @param target int, numero que debe dar como suma

     * @return Un arreglo de enteros int[], que son los numeros que cumplen con la condicion .
     */
    public static int[] twoSum(int [] nums, int target){

        /** Un map para almacenal los numeros y sus indices*/
        HashMap<Integer, Integer> map = new HashMap<>();

        /** Iteramos a traves del arreglo*/
        for(int i = 0; i < nums.length; i++){

            int complement  = target - nums[i];

            /** verificamos que el complemento se encuentre en el mapa */
            if(map.containsKey(complement)) {
                return new int[]{map.get(complement), i};
            }

            map.put(nums[i], i);
        }
        /** Si no se encuentra ninguna solucion se devuelve un arreglo vacio*/
        return new int[]{};
    }
}
package ec.telconet.prueba.boris.silva.mscomppruebaborissilva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaBorisSilvaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaBorisSilvaApplication.class, args);
	}

}

package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponseList {

    private List<Product> products;
}

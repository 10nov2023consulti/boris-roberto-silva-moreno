-- Crear la base de datos
CREATE DATABASE final;



-- Crear la tabla Usuario
CREATE TABLE Usuario (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    mail VARCHAR(100) NOT NULL,
    estado BOOLEAN NOT NULL
);

-- Crear la tabla Producto
CREATE TABLE Producto (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    cantidad INTEGER NOT NULL,
    precio DECIMAL(10, 2) NOT NULL,
    imagen VARCHAR(255),
    estado BOOLEAN NOT NULL
)

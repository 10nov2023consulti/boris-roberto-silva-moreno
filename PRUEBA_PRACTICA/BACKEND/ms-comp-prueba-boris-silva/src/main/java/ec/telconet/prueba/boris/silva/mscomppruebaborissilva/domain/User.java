package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class User {

    private Integer idUser;
    @NotEmpty
    @Pattern(regexp = "^[A-Z]+$", message = " Solo debe ser mayusculas")
    private String userName;
    @NotEmpty
    @Size(min = 8 , message = " debe tener ninimo 8 caracteres")
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[@#$%^&+=]).*$", message = " debe tener una mayuscula y un caracter especial")
    private String password;
    @NotEmpty
    @Email
    private String email;

    private boolean status;



    private List<RolUser> rolUsers;

}

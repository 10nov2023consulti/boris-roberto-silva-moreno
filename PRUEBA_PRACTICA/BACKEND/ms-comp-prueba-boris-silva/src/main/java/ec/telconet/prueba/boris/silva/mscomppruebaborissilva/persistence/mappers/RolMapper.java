package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.mappers;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.Rol;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.RolEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RolMapper {

    @Mappings({

            @Mapping(source = "name", target = "name"),
            @Mapping(source = "active", target = "active"),
    })
    Rol toRol(RolEntity rolEntity);
    List<Rol> toRoles(List<RolEntity> rolEntities);
    @InheritInverseConfiguration
    @Mappings({

    })
    RolEntity toRolEntity(Rol rol);
}
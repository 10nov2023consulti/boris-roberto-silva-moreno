import { Component, OnInit } from '@angular/core';
import { User } from '../class/user';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit{


  public user: User = new User();

  public errores: string[] = [];
 
  
  public titulo: String = "Crear Usuario";

  constructor(private userService: UserService,
    private router:Router,
    private activatedRoute: ActivatedRoute,
    ){}
  ngOnInit(): void {
    this.cargarUser();
  }
     
    public cargarUser(): void{
      this.activatedRoute.params.subscribe(params => {
        let id = params['id']
        if(id){
          this.userService.getUserById(id).subscribe((user) => this.user = user )
        }
      })
  
    }
 

  public create():  void{
  
    
    console.log(this.user);
   
    this.userService.create(this.user).subscribe(
      user => {
        this.router.navigate(['/login'])
        Swal.fire('Nuevo usuario', `Usuario ${user.userName} creado con exito`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error(err.error.errors);
      }
    )
  }

  public update(): void{
    this.userService.update(this.user).subscribe(
      user => {
        this.router.navigate(['/dashboard'])
        Swal.fire('Cliente Actualizado', `CLiente ${user.userName} actualizado con éxito!`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error(err.error.errors);
      }
    )
  }
}

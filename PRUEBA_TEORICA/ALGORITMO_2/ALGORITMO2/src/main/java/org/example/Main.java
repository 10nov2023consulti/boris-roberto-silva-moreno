package org.example;


/**
 * Este es el metodo principal del algoritmo .
 * Aqui se manda a llamar al metodo isPalindrome y se le pasa como
 * paramtro el numero a verificar
 *

 *
 */
public class Main {
    public static void main(String[] args) {

        int x1 = 121;
        /** Lamamos al metodo isPalindome y le pasamos x1*/
        System.out.println("Ejemplo #1: " + isPalindrome(x1)); // Salida: true

        // Ejemplo #2
        int x2 = -12-1;
        /** Lamamos al metodo isPalindome y le pasamos x2*/
        System.out.println("Ejemplo #2: " + isPalindrome(x2)); // Salida: false

        // Ejemplo #3
        int x3 = 010;
        /** Lamamos al metodo isPalindome y le pasamos x3*/
        System.out.println("Ejemplo #3: " + isPalindrome(x3)); // Salida : false
    }

    /**
     * Este metodo comprueba si un numero es palindrome , y devuele un boolean .
     *
     * @param x int , numero que va a ser validado.

     * @return Un boolean , si es palindrome retorna true, de lo contrario retorna false .
     */
    public static boolean isPalindrome(int x){

        /** Validamos que el numero no sea un negativo o que ternime en cero, exepto el cero*/
        if(x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }
        // -12-1   inverso  1-21-  010

        int reverded = 0;
        int original = x ;

        /** Invertimos el numero*/
        while (x > 0) {
            int digit = x % 10;
            reverded = reverded * 10 + digit;
            x /= 10;
        }
        /** verificamos si el numero original sea igual al numero invertido*/
        return original == reverded;
    }


}
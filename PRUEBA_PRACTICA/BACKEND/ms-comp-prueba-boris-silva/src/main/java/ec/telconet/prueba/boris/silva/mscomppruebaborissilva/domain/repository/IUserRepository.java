package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.repository;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    Page<User> findAll(Pageable pageable);

    User findById(int idUser);
    User findByUserName(String username);

    User save(User user);

    User update(User user);

    void deleteById(int idUser);
}
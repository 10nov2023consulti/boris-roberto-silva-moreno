package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableMethodSecurity(securedEnabled = true)
public class SecurityConfig {

    @Autowired
    private JwtFilter jwtFilter;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
                .cors(Customizer.withDefaults())
                .sessionManagement(session -> {
                    session
                            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                })
                .authorizeHttpRequests(customizeRequests -> {
                    customizeRequests
                            .requestMatchers(HttpMethod.POST, "/api/auth/**").permitAll()

                            .requestMatchers(HttpMethod.GET, "user/**").permitAll()
                            .requestMatchers(HttpMethod.POST, "user/**").permitAll()
                            .requestMatchers(HttpMethod.PUT, "user/**").permitAll()
                            .requestMatchers(HttpMethod.DELETE, "user/**").permitAll()
                            .requestMatchers(HttpMethod.GET,"/product/**").permitAll()
                            .requestMatchers(HttpMethod.POST,"/product/**").permitAll()
                            .requestMatchers(HttpMethod.PUT,"/product/**").permitAll()
                            .requestMatchers(HttpMethod.DELETE,"/product/**").permitAll()
                            .requestMatchers(HttpMethod.POST, "/compra/**").permitAll()

                            .anyRequest()
                            .authenticated();
                })
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean("clienteRest")
    public RestTemplate registrarRestTemplate() {
        return  new RestTemplate();
    }

}
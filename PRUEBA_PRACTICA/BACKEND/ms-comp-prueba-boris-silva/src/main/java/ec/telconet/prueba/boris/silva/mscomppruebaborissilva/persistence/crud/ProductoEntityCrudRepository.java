package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductoEntityCrudRepository extends JpaRepository<ProductoEntity, Integer> {

    List<ProductoEntity> findTop5ByEstado(boolean estado);
}

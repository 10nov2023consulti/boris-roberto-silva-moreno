package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud.UserCrudRepository;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserCrudRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUserName(username).orElseThrow(
                () -> new UsernameNotFoundException("User " + username + "not found")
        );

        System.out.println("El user entity");


        /* String[] roles = userEntity.getRolUserEntities().stream().map(RolUserEntity::getRolEntity).toArray(String[]::new);
         */

      /*  List<String> roles = userEntity.getRolUsers()
                        .stream()
                                .map(rolUserEntity -> rolUserEntity.getIdRol().getName())
                                        .toList();*/

        System.out.println("Busco el rol ");
        return User.builder()
                .username(userEntity.getUserName())
                .password(userEntity.getPassword())
                .roles("ADMIN")
                .accountLocked(false)
                .build();
    }
}
package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain;

import lombok.Data;

@Data
public class RolUser {

    private String idRol;

    private Rol rol;
}
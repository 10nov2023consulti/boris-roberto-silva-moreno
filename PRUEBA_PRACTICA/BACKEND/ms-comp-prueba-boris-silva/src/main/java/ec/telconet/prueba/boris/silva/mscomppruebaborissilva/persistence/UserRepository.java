package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.repository.IUserRepository;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud.UserCrudRepository;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.UserEntity;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository implements IUserRepository {
    @Autowired
    private UserCrudRepository userCrudRepository;

    /*@Autowired
    private RolCrudRepository rolCrudRepository;*/

    @Autowired
    private UserMapper mapper;


    @Override
    public List<User> findAll() {

        return mapper.toUsers(userCrudRepository.findAll());
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        Page<UserEntity> userEntityPage = userCrudRepository.findAll(pageable);

        List<User> users = mapper.toUsers(userEntityPage.getContent());
        return new PageImpl<>(users, pageable, userEntityPage.getTotalElements());
    }


    @Override
    public User findById(int idUser) {
        UserEntity userEntity = userCrudRepository.findByIdUser(idUser).get();

        return mapper.toUser(userEntity);
    }

    @Override
    public User findByUserName(String username) {

        return mapper.toUser(userCrudRepository.findByUserName(username).get());
    }

    @Override
    public User save(User user) {



        UserEntity userEntity = mapper.toUserEntity(user);



        userEntity.getRolUserEntities().forEach(rolUserEntity -> rolUserEntity.setUserEntity(userEntity));
        return mapper.toUser(userCrudRepository.save(userEntity));
    }

    @Override

    public User update(User user) {
        UserEntity userEntity = userCrudRepository.getReferenceById(user.getIdUser());
        userEntity.setUserName(user.getUserName());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail(user.getEmail());
        userEntity.setEstado(user.isStatus());


        return mapper.toUser(userCrudRepository.save(userEntity));
    }

    @Override
    public void deleteById(int idUser) {
        UserEntity userEntity = userCrudRepository.getReferenceById(idUser);
        userEntity.setEstado(false);
        userCrudRepository.save(userEntity);

    }
}
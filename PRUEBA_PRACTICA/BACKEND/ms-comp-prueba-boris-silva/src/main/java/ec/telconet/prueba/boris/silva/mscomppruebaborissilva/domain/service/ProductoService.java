package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.crud.ProductoEntityCrudRepository;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.ProductoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    private ProductoEntityCrudRepository productoEntityCrudRepository;

    public List<ProductoEntity> getALl(){
        return productoEntityCrudRepository.findTop5ByEstado(true);
    }

    public Page<ProductoEntity> findAll(Pageable pageable){
        return productoEntityCrudRepository.findAll(pageable);
    }

    public Optional<ProductoEntity > findById(int idProducto){
        return productoEntityCrudRepository.findById(idProducto);
    }

    public ProductoEntity save(ProductoEntity productoEntity){
        return productoEntityCrudRepository.save(productoEntity);
    }

    public ProductoEntity update(ProductoEntity productoEntity){
        ProductoEntity productoEntity1 = productoEntityCrudRepository.getReferenceById(productoEntity.getId());
        productoEntity1.setNombre(productoEntity.getNombre());
        productoEntity1.setPrecio(productoEntity.getPrecio());
        productoEntity1.setImagen(productoEntity.getImagen());
        productoEntity1.setCantidad(productoEntity.getCantidad());
        productoEntity1.setEstado(productoEntity.isEstado());
        return productoEntityCrudRepository.save(productoEntity1);
    }

    public void deleteById(int idProducto){

        ProductoEntity productoEntity = productoEntityCrudRepository.getReferenceById(idProducto);

        productoEntity.setEstado(false);

         productoEntityCrudRepository.save(productoEntity);
    }
}

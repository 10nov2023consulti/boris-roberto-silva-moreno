import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { User } from '../../class/user';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {

  @Input()
 public user!: User ;


 
  @ViewChild('tablaUsuarios') tablaUsuarios!: ElementRef;

  exportToPDF() {
    const content: HTMLElement = this.tablaUsuarios.nativeElement;
  
    html2canvas(content, { scale: 3, logging: true }).then(canvas => {
      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF();
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = pdf.internal.pageSize.getHeight();
      pdf.addImage(imgData, 'PNG', 0, 0, 100 ,50);
      pdf.save('usuarios.pdf');
    });
  }
}


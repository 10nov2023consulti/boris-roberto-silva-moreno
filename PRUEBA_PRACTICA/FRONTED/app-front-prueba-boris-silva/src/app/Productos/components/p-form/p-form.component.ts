import { Component } from '@angular/core';

import { ProductoService } from '../../services/producto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Content } from '../../interfaces/producto.interface';
import Swal from 'sweetalert2';
import { Product } from '../../interfaces/product';

@Component({
  selector: 'app-p-form',
  templateUrl: './p-form.component.html',
  styleUrls: ['./p-form.component.css']
})
export class PFormComponent {

  public producto!:  Content;
  public product : Product = new Product();

  public errores: string[] = [];
 
  
  public titulo: String = "Crear Productos";

  constructor(private productoService: ProductoService,
    private router:Router,
    private activatedRoute: ActivatedRoute,
    ){}

  ngOnInit(): void {
    console.log('form');
    this.cargarProducto();
  }
     
    public cargarProducto(): void{
      this.activatedRoute.params.subscribe(params => {
        let id = params['id']
        if(id){
          this.productoService.getProductoById(id).subscribe((producto) => this.product = producto )
        }
      })
  
    }
 

  public create():  void{
  
    
    console.log(this.producto);
   
    this.productoService.create(this.product).subscribe(
      producto => {
        this.router.navigate(['/producto'])
        Swal.fire('Nuevo Producto', `Producto ${producto.nombre} creado con exito`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error(err.error.errors);
      }
    )
  }

  public update(): void{
    this.productoService.update(this.product).subscribe(
      producto => {
        this.router.navigate(['/dashboard'])
        Swal.fire('Producto Actualizado', `Producto ${producto.nombre} actualizado con éxito!`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error(err.error.errors);
      }
    )
  }
}

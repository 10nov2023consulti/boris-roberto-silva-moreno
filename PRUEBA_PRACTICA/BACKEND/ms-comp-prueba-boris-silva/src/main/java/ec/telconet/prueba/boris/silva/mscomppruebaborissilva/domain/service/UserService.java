package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.RolUser;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.User;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public Page<User> finAll(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    public User findById(int idUser){
        return userRepository.findById(idUser);
    }
    public User findByUserName(String username){
        return userRepository.findByUserName(username);
    }

    public User save(User user){
        List<RolUser> rolUsers = new ArrayList<>();
        RolUser rolUser = new RolUser();
        rolUser.setIdRol("USER");
        rolUsers.add(rolUser);

        user.setRolUsers(rolUsers);

        String passEncoder =  passwordEncoder.encode(user.getPassword());
        user.setPassword(passEncoder);
        return userRepository.save(user);
    }

    public void importUsers(MultipartFile file) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                // Dividir la línea en columnas utilizando la coma como separador
                String[] data = line.split(",");

                // Verificar si la línea tiene el formato correcto
                if (data.length == 4) {
                    // Crear un nuevo usuario y guardarlo en la base de datos
                    User user = new User();
                    user.setUserName(data[0]);

                    user.setPassword(data[1]);
                    user.setStatus(Boolean.parseBoolean(data[2]));
                    user.setEmail(data[3]);

                    userRepository.save(user);
                } else {
                    // Loggear un mensaje de error o lanzar una excepción si el formato no es el esperado
                    System.err.println("Error: Formato de línea incorrecto - " + line);
                }
            }
        }
    }

    public User update(User user){
        return userRepository.update(user);
    }

    public void deleteById(int idUser){
        userRepository.deleteById(idUser);
    }
}
package ec.telconet.prueba.boris.silva.mscomppruebaborissilva.web.controller;

import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.domain.service.ProductoService;
import ec.telconet.prueba.boris.silva.mscomppruebaborissilva.persistence.entity.ProductoEntity;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("/all")
    public ResponseEntity<List<ProductoEntity>> getAll(){
        return new ResponseEntity<>(productoService.getALl(), HttpStatus.OK) ;
    }

    @GetMapping("/last/{page}")
    public ResponseEntity<Page<ProductoEntity>> findAll(@PathVariable("page") int page){
        Pageable pageable = PageRequest.of(page, 5);

        return new ResponseEntity<>(productoService.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ProductoEntity> findById(@PathVariable("id") int idProducto){

        return new ResponseEntity<>(productoService.findById(idProducto).get(), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@Valid @RequestBody ProductoEntity productoEntity, BindingResult result){

        Map<String, Object> response = new HashMap<>();

        System.out.println(result.getAllErrors());

        if (result.hasErrors()){

            List<String> errors = result.getFieldErrors().stream()
                    .map(e -> "El campo " + e.getField()+" "+ e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(productoService.save(productoEntity), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@Valid @RequestBody ProductoEntity productoEntity, BindingResult result){

        Map<String, Object> response = new HashMap<>();
        if (result.hasErrors()){

            List<String> errors = result.getFieldErrors().stream()
                    .map(e -> "El campo " + e.getField()+" "+ e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(productoService.update(productoEntity), HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int idProducto){
        productoService.deleteById(idProducto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
